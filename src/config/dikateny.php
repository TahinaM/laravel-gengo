<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Locales
    |--------------------------------------------------------------------------
    |
    | Contains an array with the applications locales default sources.
    |
    */
    'locales' => [
        'lc_src'  =>'fr',
        'lc_tgt'  =>'en'
    ],

    /*
    |--------------------------------------------------------------------------
    | Gengo keys
    |--------------------------------------------------------------------------
    |
    | Contains an array with the Gengo api keys
    |
    */
    'api'     => [
        'key'           => env('GENGO_PUBLIC_KEY', 'apikey'),
        'privatekey'    => env('GENGO_PRIVATE_KEY', 'apiprivatekey'),
        'callback'      => env('GENGO_CALLBACK_URL', 'apicallbackurl')
    ],

    'domain'  => [
        'callback'      => env('GENGO_CALLBACK_DOMAIN', 'domaincallback'),
        'drequest'      => env('GENGO_REQUEST_DOMAIN', 'domainrequest')
    ]
];
