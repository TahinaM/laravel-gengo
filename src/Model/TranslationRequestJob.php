<?php 

namespace Tokido\Dikateny\Model;

Use Illuminate\Database\Eloquent\Model;

class TranslationRequestJob extends Model
{
    protected $fillable = [
        'translation_request_order_id',
        'order_id',
        'job_id',
        'slug',
        'body_src',
        'lc_src',
        'lc_tgt',
        'unit_count',
        'tier',
        'credits',
        'currency',
        'status',
        'eta',
        'ctime',
        'auto_approve',
        'position',
        'lc_tgt',
        'callback_url',
    ];

    public $timestamps = false;

    public function order()
    {
        return $this->belongsTo('Tokido\Dikateny\Model\TranslationRequestOrder', 'translation_request_order_id');
    }

    public function callbackResponses()
    {
        return $this->hasMany('Tokido\Dikateny\Model\TranslationRequestJobResponse', 'translation_request_job_id');
    }

    public function translationRevisions()
    {
        return $this->hasMany('Tokido\Dikateny\Model\TranslationRequestJobRevision', 'translation_request_job_id');
    }

    public function translationsComments()
    {
        return $this->morphMany('Tokido\Dikateny\Model\TranslationRequestOrder', 'commentable');
    }
}