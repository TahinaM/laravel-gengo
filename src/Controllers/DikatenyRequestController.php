<?php

namespace Tokido\Dikateny\Controllers;

use App;
use Gengo;
use Illuminate\Http\Request;
use Tokido\Dikateny\Dikateny;
use Illuminate\Support\Facades\Log;
use Illuminate\Routing\Controller as BaseController;
use Tokido\Dikateny\Model\TranslationRequestJob;
use Tokido\Dikateny\Model\TranslationRequestJobResponse;
use Tokido\Dikateny\Model\TranslationRequestOrder;
use Tokido\Dikateny\Model\TranslationRequestJobComment;

class DikatenyRequestController extends BaseController
{

    public function __construct(
        Dikateny $translator
    ){
        $this->translator = $translator;
    }

    public function cancelAvailableTranslationRequestByOrderId($id)
    {
        if($id) {
            $order = TranslationRequestOrder::find($id);
            if($order) {
                $status =  $this->translator->cancelOrderByID($order->order_id);

                if( is_array($status) && $status['opstat'] == 'ok') {
                    
                    $order->status = 'canceled';
                    foreach ($order->jobs as $job) {
                        $job->status = 'canceled';
                        $job->save();
                    }
                    $order->save();

                    return redirect()->back();
                }
            }
        }

        return redirect()->back();
    }

    public function requestRevisionByOrderId($id, $back_route = null)
    {
        $order = TranslationRequestOrder::find($id);


        return view('dikateny::revision', [
            'order'     => $order,
            'back_route'  => $back_route
        ]);
    }

    public function submitRevisionByOrderId(Request $request)
    {
        $comment = $request->get('comment');
        if(!$comment ||empty($comment)) 
            return redirect()->back();

        $id = $request->get('id');
        $jobs = array() ;
        if($comment && $id) {
            $order = TranslationRequestOrder::find($id);
            $jobList = $order->jobs->pluck('job_id')->toArray();
            if(sizeof($jobList)> 0) {
                foreach ($jobList as $job_id) {
                    array_push($jobs, ['job_id' => $job_id]);
                }

                $revision = $this->translator->reviseMultipleJobByIds($jobs, $comment);
                return \Redirect::to(base64_decode($request->get('back_route')));
            }
        }

        return \Redirect::to(base64_decode($request->get('back_route')));
    }

    public function buildRejectFormatData($comments, $captcha)
    {
        $result = array();
        foreach ($comments as $key => $value) {
            $rejectableData = array('jobId' => $key);
            $rejectableData['captcha'] = $captcha[$key];
            $rejectableData['comment'] = $value;
            array_push($result, $rejectableData);
        }

        return $result;
    }

    public function requestRejectionByOrderId($id, $back_route = null)
    {
        $order = TranslationRequestOrder::find($id);
        $responses = array();

        foreach ($order->jobs as $job) {
            $responses[$job->slug] =  $job->callbackResponses()->orderBy('created_at', 'desc')->first();
        }

        return view('dikateny::rejection', [
            'order'         => $order,
            'responses'     => $responses,
            'back_route'    => $back_route
        ]);
    }

    public function rejectRevisionByOrderId(Request $request)
    {
        $datas = $request->all();
        //flyingvalidation @todo
        /*$validateData = $request->validate(function () use ($datas) {
            
            $responses = array();
            if(in_array($datas, 'comments')) {
                foreach ($datas['comments'] as $commentaires) {
                    $responses['comments.' . $commentaires] = 'required';
                    $responses['captcha.' . $commentaires] = 'required';
                }
            }
            $responses['global_comment'] = 'required';
            return $responses;
        });*/

        $rejectionRequest = $this->buildRejectFormatData($datas['comments'], $datas['captcha']);
        $response = $this->translator->rejectMultipleJobs($rejectionRequest, $request->get('global_comment'));

        if($response && is_array($response) && $response['opstat'] == 'ok') {
            if($this->updateJobStatus($response['response'])) 
                return \Redirect::to(base64_decode($request->get('back_route')));

        }

        return redirect()->back();
    }

    public function updateJobStatus($datas)
    {
        if($datas && is_array($datas)) {
            foreach ($datas['jobs'] as $jobResponse) {
                $job = TranslationRequestJob::where('job_id', $jobResponse['job_id'])->first();
                if($job) {
                    $order = $job->order;
                    $job->status = 'hold';
                    $job->save();
                    $order->status = 'hold';
                    $order->save();
                }
            }

            return true;
        }
        return false;
    }

    public function requestApprovalJobsById($id, $applytranslation , $back_route = null)
    {
        $order = TranslationRequestOrder::find($id);
        
        return view('dikateny::approval', [
            'order'             => $order,
            'applytranslation'  => $applytranslation,
            'back_route'        => $back_route
        ]);
    }


    public function previewResponseById($id, $back_route = null)
    {
        $response = TranslationRequestJobResponse::find($id);
        
        return view('dikateny::preview', [
            'response'      => $response,
            'back_route'    => $back_route
        ]);
    }

    public function approveRevisionByOrderId(Request $request)
    {
        $datas = $request->all();
        $id = $request->get('id');
        $order = TranslationRequestOrder::find($id);
        
        if(!in_array('public', $datas))
            $datas['public'] = false;

        $response = $this->translator->approveMultipleJobsWithSameNote($datas['jobIds'], $datas['rating'] , $datas['for_translator'], $datas['for_mygengo'], $datas['public']);

        if($order) {
            $objectclass = $order->translatable_type;

            if($datas['applytranslation'] == 1) {
                $object = $objectclass::find($order->translatable_id);
                $translations = $object->getApprovedTranslatableValues();

                $status = $object->applyTranslations($translations);

                return \Redirect::to(base64_decode($request->get('back_route')));
                
            }
        }

        //return $response;
        return \Redirect::to(base64_decode($request->get('back_route')));
    }
}