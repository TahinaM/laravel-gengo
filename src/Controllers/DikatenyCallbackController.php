<?php

namespace Tokido\Dikateny\Controllers;

use App;
use Gengo;
use Illuminate\Http\Request;
use Tokido\Dikateny\Dikateny;
use Illuminate\Support\Facades\Log;
use Illuminate\Routing\Controller as BaseController;
use Tokido\Dikateny\Model\TranslationRequestJob;
use Tokido\Dikateny\Model\TranslationRequestJobResponse;
use Tokido\Dikateny\Model\TranslationRequestOrder;
use Tokido\Dikateny\Model\TranslationRequestJobComment;

class DikatenyCallbackController extends BaseController
{

    public function __construct(
        Dikateny $translator
    ){
        $this->translator = $translator;
    }

    public function callback(Request $request)
    {
        $response = $request->all();


        if(isset($response) && is_array($response) && array_key_exists('job', $response)) {
            $this->saveCallbackRequestAsResponse($response);
        }

        if(isset($response) && is_array($response) && array_key_exists('comment', $response)) {
            $this->saveCallbackRequestAsComment($response);
        }
    }

    public function saveCallbackRequestAsResponse($response)
    {
        $responseData = json_decode($response['job'], true);


        if(is_array($responseData) && array_key_exists('status', $responseData)) {

            if(array_key_exists('status', $responseData) && $responseData['status'] == 'available') {
                $job = $this->saveAvailableJobsForOrder($responseData, $responseData['order_id']);
            }

            if(array_key_exists('status', $responseData) && ($responseData['status'] != 'available')) {
                $job = $this->updateJobStatus($responseData, $responseData['status']);
            }

            if(array_key_exists('status', $responseData) && $responseData['status'] == 'reviewable') {
                $job = TranslationRequestJob::where('job_id', $responseData['job_id'])->first(); 
                $job->status = 'reviewable';
                
                $order = TranslationRequestOrder::find($job->translation_request_order_id);
                
                $jobResponse = new TranslationRequestJobResponse($responseData);
                $job->callbackResponses()->save($jobResponse);
                
                if($order->wait_validation != true) {
                    if($this->isCompleted($order)) {
                        $job->status = 'approved';
                        $order->status = 'approved';
                        \Log::info('completed');
                        \Log::info($order);
                        $this->completeAndApplyTranslation($order);
                    }
                } else {
                    $order->status = $responseData['status'];
                }

                $order->save();
                $job->save();

            }

        }
    }

    public function updateJobStatus($responseData, $status)
    {
        $job = TranslationRequestJob::where('job_id', $responseData['job_id'])->first();
        if($job) {
            $job->status = $status;
            /*update order status too*/
            $order = TranslationRequestOrder::find($job->translation_request_order_id);
            if($order) {
                $order->status = $status;
                $order->save();
            }
            $job->save();
        }

        return $job;
    }

    public function saveAvailableJobsForOrder($responseData, $order_id)
    {
        $order = TranslationRequestOrder::where('order_id', $order_id)->first();
        if($order) {
            $jobs = $this->requestJobFromOrder($order);
            if($jobs && $jobs != null && $jobs['opstat'] && $jobs['opstat'] == 'ok' && $jobs['response'] && $jobs['response']['jobs']) {
                foreach ($jobs['response']['jobs'] as $job) {
                    $existingJob = TranslationRequestJob::where('job_id', $job['job_id'])->first();
                    if(!$existingJob) {
                        $requestJob = new TranslationRequestJob($job);
                        $order->jobs()->save($requestJob);
                    }
                }    
            }
            $order->status = "available";
            $order->save();
        }

        return $job;
    }

    public function requestJobFromOrder($order)
    {
        $orderData = $this->translator->getOrder($order->order_id);
        $orderIds  = $this->translator->getAllJobIdsAndStatusFromOrder($orderData, ['jobs_available']);

        if($orderIds && sizeof($orderIds) > 0) {
            $response = $this->translator->getJobsByIds($orderIds);
        } else {
            $response = null;
        }
        return $response;   
    }

    public function saveCallbackRequestAsComment($response)
    {
        $responseData = json_decode($response['comment'], true);
        if(is_array($responseData) && array_key_exists('body', $responseData) && array_key_exists('job_id', $responseData)) {
            $job = TranslationRequestJob::where('job_id', $responseData['job_id'])->first();
            $comment = new TranslationRequestJobComment($responseData);
            $job->translationsComments()->save($comment);
        }
    }

    public static function getCallbackStatus()
    {
        return array(
            'queued',
            'available',
            'pending',
            'reviewable',
            'approved',
            'revising',
            'rejected',
            'canceled',
            'hold'
        );
    }

    public function completeAndApplyTranslation($order) 
    {
        $order->jobs()->update(['status' => 'approved']);
        $order->status = 'approved';
        $order->save();

        foreach ($order->jobs as $approvableJob) {
            $this->updateJobAndResponseStatus($approvableJob);
        }

        $objectclass = $order->translatable_type;
        $object = $objectclass::find($order->translatable_id);
        $translations = $object->getApprovedTranslatableValues();

        $status = $object->applyTranslations($translations);
        
        return true;
    }

    public function updateJobAndResponseStatus ($approvableJob)
    {
        $responseJob = $approvableJob->callbackResponses()->orderBy('created_at', 'desc')->first();
        if($responseJob) {
            $responseJob->status = 'approved';
            if (!$responseJob->save()) 
                \Log::info("Failed to save!");
        }
    }

    public function isCompleted($order)
    {
        foreach ($order->jobs as $job) {
            if($job->status != 'reviewable' && $job->status != 'approved')
                return false;
        }

        return true;
    }
}