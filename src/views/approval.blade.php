<form method="post" action="{{route('dikateny.translations.approval.submit')}}">
    
    @foreach($order->jobs->pluck('job_id')->toArray() as $jobIds)
    <input type="hidden" name="jobIds[]" value="{{$jobIds}}">
    @endforeach
    <input type="hidden" name="back_route" value="{{$back_route}}">
    <input type="hidden" name="applytranslation" value="{{$applytranslation}}">
    <input type="hidden" name="id" value="{{$order->id}}">


    <label>Commentaire pour le traducteur</label>
    <input type="text" name="for_translator" ><br>

    <label>Rendre public le commentaire</label>
    <input type="checkbox" name="public" value="1"><br>

    <label>Commentaire pour le compte gengo</label>
    <input type="text" name="for_mygengo" ><br>

    <label>Note</label><br>
    @for($i = 1; $i<=5; $i++)
        <label>{{$i}}</label>
        <input type="radio" name="rating" value="{{$i}}" @if($i == 3) checked="checked" @endif ><br>
    @endfor

    <input type="submit" value="Submit">
    <a href="{{$back_route ? base64_decode($back_route) : '#'}}">Annuler </a>
</form> 
