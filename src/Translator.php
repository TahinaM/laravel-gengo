<?php

namespace Tokido\Dikateny;

use App;
use Illuminate\Http\Request;
use Tokido\Dikateny\Dikateny;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Tokido\Dikateny\Model\TranslationRequestJob;
use Tokido\Dikateny\Model\TranslationRequestOrder;
use Tokido\Dikateny\Model\TranslationRequestJobComment;
use Tokido\Dikateny\Model\TranslationRequestJobRevision;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder as QueryBuilder;

trait Translator
{
    protected $defaultLocale;

    protected static $dikateny; 

    public static function bootTranslator()
    {
        static::$dikateny = \App::make(Dikateny::class, array( 
                config('dikateny.api.key'),
                config('dikateny.api.privatekey')
            )
        );
    }


    public static function getTranslatables()
    {
        /*need reflection to work */
        $instance = new self();
        return $instance->autoTranslatables;
    }

    public function serializeTranslatablesValues()
    {

        $serialized = array();
        foreach ($this->autoTranslatables as $translatableValues) {
            $serialized[$translatableValues] = $this->$translatableValues;
        }
        return $serialized;
    }

    public function submitTranslationOrder( $tier = "standard", $force = 1, $lc_src = "fr", $lc_tgt = "en", $type = "text")
    {
        $jobList = array();
        foreach ($this->autoTranslatables as $translatableValues) {
            $job = self::$dikateny->createJob(
                $translatableValues, 
                $this->$translatableValues,
                $type,
                $tier,
                $force,
                $lc_src,
                $lc_tgt
            );

            array_push($jobList, $job);
        }
        $order = self::$dikateny->postJobs($jobList);
        
        return $order;   
    }

    public function saveOrder($order, $needValidation = false, $translationForce = 1)
    {
        $requestOrder = null;
        
        if($order && $order['opstat'] && $order['opstat'] == 'ok') {
            $requestOrder =  new TranslationRequestOrder($order['response']);
            $requestOrder->wait_validation = $needValidation;
            $requestOrder->translation_force = $translationForce;
            $requestOrder->save();
        }
        
        return $requestOrder;
    }

    /*public function requestJobFromOrder($order)
    {
        $orderData = self::$dikateny->getOrder($order->order_id);
        $orderIds = self::$dikateny->getAllJobIdsAndStatusFromOrder($orderData, ['jobs_available']);
        if($orderIds && sizeof($orderIds) > 0) {
            $response = self::$dikateny->getJobsByIds($orderIds);
        } else {
            $response = null;
        }
        return $response;   
    }*/

    public function requestTranslations($needValidation = false, $tier = "standard", $force = 1, $lc_src = "fr", $lc_tgt = "en")
    {
        $order = $this->submitTranslationOrder($tier, $force, $lc_src, $lc_tgt);
        $requestOrder = $this->saveOrder($order, $needValidation, $force);
        $this->translationRequestOrders()->save($requestOrder);

        /*useless; get from the callback*/
        
        /*$jobs = $this->requestJobFromOrder($requestOrder);
        
        if($jobs && $jobs != null && $jobs['opstat'] && $jobs['opstat'] == 'ok' && $jobs['response'] && $jobs['response']['jobs']) {
            foreach ($jobs['response']['jobs'] as $job) {
                $requestJob = new TranslationRequestJob($job);
                $requestOrder->jobs()->save($requestJob);
            }    
        }*/
    }

    public static function requestTranslationById($id, $needValidation = false , $tier = "standard", $force = 1, $lc_src = "fr", $lc_tgt = "en")
    {

        $object = self::find($id);
        $object->requestTranslations($needValidation, $tier, $force, $lc_src, $lc_tgt);
    }

    public function checkRequestedTranslationStatus()
    {
           
    }

    public function cancelLastRequestOrderById($orderId)
    {
        return self::$dikateny->cancelOrderByID($orderId);
    }

    public static function cancelRequestJobById($jobId)
    {
        $response =  self::$dikateny->cancelJobByID($jobId);
        if(is_array($response) && array_key_exists('opstat', $response) && $response['opstat'] == 'ok') {
            $job = TranslationRequestJob::where('job_id', $jobId)->first();
            if($job) {
                $job->status = "cancelled";
                $job->save();
            }
            return $job;
        }

        return null;
    }

    public static function cancelRequestJobByIds(array $jobIds)
    {
        $responses = array();

        foreach ($jobIds as $jobId) {
            $response = self::cancelRequestJobById($jobId);
            if ($response != null)
                array_push($responses, $response);
        }
        return $responses;
    }

    public function cancelAllCurrentRequestOrder()
    {
        $cancelledJobsForOrder = array();
        
        /*For last Request Only :*/
        $requestOrder = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        
        //foreach ($this->translationRequestOrders as $requestOrder) {
        $response = $this->cancelLastRequestOrderById($requestOrder->order_id);
        if(is_array($response) && array_key_exists('opstat', $response) && $response['opstat'] == 'ok') {
            if(is_array($response['response']) && array_key_exists('order', $response['response']) && array_key_exists('jobs_cancelled', $response['response']['order']) && sizeof($response['response']['order']['jobs_cancelled'])>0) {
                foreach ($response['response']['order']['jobs_cancelled'] as $cancelled) {
                    $job = TranslationRequestJob::where('job_id', $cancelled)->first();
                    if($job) {
                        $job->status = "cancelled";
                        $job->save();
                        array_push($cancelledJobsForOrder, $job);
                    }
                }
            }
        }
        //}
        return $cancelledJobsForOrder;
    }

    public static function cancelAllCurrentRequestOrderByObjectId($id)
    {   
        $object = self::find($id);
        $cancelledJobs = $object->cancelAllCurrentRequestOrder();

        return $cancelledJobs;
    }

    public function postCommentOnLastOrder($body)
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        return self::postOrderCommentByOrderId($order->order_id, $body);
    }

    public static function postCommentOnLastOrderById($id, $body)
    {
        $object = self::find($id);
        if($object) {
            return $object->postCommentOnLastOrder($body);
        }
        return null;
    }

    public static function postOrderCommentByOrderId($orderId, $body)
    {
        $response = self::$dikateny->commentOrder($orderId, $body);
        if(is_array($response) && $response['opstat'] == 'ok') {
            $comment = new TranslationRequestJobComment([
                'body'      => $body,
                'author'    => 'admin',
                'order_id'  => $orderId
            ]);

            $order = TranslationRequestOrder::where('order_id', $orderId)->first();
            $order->translationsComments()->save($comment);

            return $comment;
        } 
        return null;
    }

    public static function postJobCommentByJobId($jobId, $body)
    {
        $response = self::$dikateny->commentJob($jobId, $body);
        if(is_array($response) && $response['opstat'] == 'ok') {
            $comment = new TranslationRequestJobComment([
                'body'      => $body,
                'author'    => 'admin',
                'job_id'  => $jobId
            ]);

            $job = TranslationRequestJob::where('job_id', $jobId)->first();
            $job->translationsComments()->save($comment);

            return $comment;
        } 
        return null;
    }

    /*single revision for multiple Jobs*/
    public static function reviseSingleJobById($jobId, $body)
    {
        $response = self::$dikateny->reviseJob($jobId, $body);
        if(is_array($response) && $response['opstat'] == 'ok') {
            $revisionData = self::saveRevisionsComment($jobId, $body);
            $job = TranslationRequestJob::where('job_id', $jobId)->first();
            if($job) {
                $job->status = 'revising';
                $job->save();

                $job->translationRevisions()->save($revisionData);
            }

            return $revisionData;
        }
        return null;
    }

    /*Single revision for multiple jobs*/
    public static function reviseMultipleJobByIds(array $jobIds, $body)
    {
        $responses = array();
        $response = self::$dikateny->reviseJobs($jobIds, $body);
        /*check for each response in array*/
        if($response && is_array($response)) {
            foreach ($jobIds as $job) {
                $revisionData = self::saveRevisionsComment($job['job_id'], $body);
                $job = TranslationRequestJob::where('job_id', $job['job_id'])->first();
                if($job) {
                    $job->translationRevisions()->save($revisionData);
                    array_push($responses, $revisionData);
                }
            }
        }
        return $responses;
    }

    /*multiple revisions with multiple comments*/
    public static function reviseJobsWithComments(array $jobs)
    {
        $responses = array();
        foreach ($jobs as $jobId => $comment) {
            $response = self::$dikateny->reviseJob($jobId, $comment);
            array_push($responses, $response);
        }
        return $responses;
    }

    public static function saveRevisionsComment($job_id, $comment)
    {
        $revision = new TranslationRequestJobRevision(
            [
                'job_id'    => $job_id,
                'comment'   => $comment
            ]
        );
        return $revision;
    }

    public static function approveSingleJob($job_id, $rating = 3 , $for_translator = null, $for_mygengo = null, $public = false)
    {
        /*save as approved*/
        $job = TranslationRequestJob::where('job_id', $job_id)->first();
        $response = self::$dikateny->approveJob($job_id, $rating , $for_translator, $for_mygengo, $public);
        if($response && $response['opstat'] == 'ok') {
            $job->status = 'approved';
            $job->save();
        }
        return $response;
    }

    /*approve multiple jobs with same params*/
    public static function approveMultipleJobsWithSameNote($jobIds, $rating = 3 , $for_translator = null, $for_mygengo = null, $public = false)
    {
        $approvals = array();
        foreach ($jobIds as $jobId) {
            $approval = self::$dikateny->buildJobApprovalArgs($jobId, $rating, $for_translator, $for_mygengo, $public);
            array_push($approvals, $approval);
        }

        return self::$dikateny->approveMultipleJobs($approvals);
    } 

    /*approve multiple jobs with different params*/
    public static function approveMultipleJobsWithdiffNote($jobsApprovals)
    {
        $approvals = array();
        foreach ($jobsApprovals as $jobsApproval) {
            extract($jobsApproval);
            array_push($approvals, self::$dikateny->buildJobApprovalArgs($jobId, $rating, $for_translator, $for_mygengo, $public));
        }

        return self::$dikateny->approveMultipleJobs($approvals);
    }

    /*Reject Single Job*/
    public static function rejectSingleJob($job_id,$comment, $captcha, $follow_up = "requeue", $reason = "quality")
    {

        $job = TranslationRequestJob::where('job_id', $job_id)->first();
        $response =  self::$dikateny->rejectSingleJob($job_id,$comment, $captcha, $follow_up, $reason);
        if($response && $response['opstat'] == 'ok') {
            $job->status = 'rejected';
            $job->save();
        }
        return $response;
    } 

    /*Public function reject multiple Jobs*/
    public static function rejectMultipleJobs($jobsRejectRequests, $comment)
    {
        $requests = array();

        foreach ($jobsRejectRequests as $jobsRejectRequest) {
            extract($jobsRejectRequest);
            array_push($requests, self::$dikateny->builtRejectJobRequests($jobId, $comment, $captcha));
        }

        return self::$dikateny->rejectMultipleJob($requests, $comment);
    }      

    /*archive single Job*/
    public static function archiveSingleJob($job)
    {
        return self::$dikateny->archiveSingleJob($job);
    }

    public static function archiveMultipleJobs($jobIds)
    {
        return self::$dikateny->archiveMultipleJobs($jobIds);
    } 

    public static function getTranslationTierList()
    {
        return array(
            'standard'  => 'Standard',
            'pro'       => 'Pro'
        );
    }

    public static function getTranslationForce()
    {
        $force = array();

        for ($i=1; $i <= 10; $i++) { 
            $force[$i] = $i;
        }

        return $force;
    }

    public function checkCurrentStatusTranslation()
    {
        $currentTranslationRequest = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($currentTranslationRequest) {
            if($currentTranslationRequest->jobs->count() > 1) {
                $initial = null;
                foreach ($currentTranslationRequest->jobs as $job) {
                    if($job->status == 'pending')
                        return 'pending';   

                    if($job->status == 'revising')
                        return 'revising';                 
                }
                return $currentTranslationRequest->status;
            } else {
                return $currentTranslationRequest->status;
            }
        }
        return 'aucune';
    }

    public function checkCurrentApprovabilityStatus()
    {
        $currentTranslationRequest = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($currentTranslationRequest) {
            return $currentTranslationRequest->wait_validation;
        }
        return false;
    }

    public function checkValidTranslationsLanguages()
    {
        return array(

        );
    }

    public function setLanguageCompatibility()
    {
        return array(

        );
    }

    public function getLanguageSrc()
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($order) {
            if($order->jobs()->first())
                return $order->jobs()->first()->lc_src;
        }
        return null;
    }

    public function getLanguageTgt()
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($order) {
            if($order->jobs()->first())
                return $order->jobs()->first()->lc_tgt;
        }
        return null;
    }

    public function getIsValidationNeeded()
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($order) {
            return $order->wait_validation;
        }
        return false;
    }

    public function getTranslationForceTranslation()
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($order) {
            return $order->translation_force;
        }
        return 1;
    }

    public function getTranslationQuality()
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($order) {
            if($order->jobs()->first())
                return $order->jobs()->first()->tier;
        }
        return null;  
    }

    public function GetAppliableReviewableTranslations()
    {
        return $this->getLastAppliableTranslations();
    }

    public static function GetAppliableReviewableTranslationsById($id)
    {
        $object = self::find($id);
        if($object) {
            return $object->GetAppliableReviewableTranslations();
        }

        return array();
    }

    public function getActiveOrderById()
    {
        return $this->translationRequestOrders()->orderBy('created_at', 'desc')->first()->id;
    }

    public function getLastAppliableTranslations()
    {
        $translationRequest = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        $translations = array();

        if($translationRequest) {
            foreach ($translationRequest->jobs as $job) {
                $response = $job->callbackResponses()->orderBy('created_at', 'desc')->first();
                if($response) {
                    array_push($translations, array('translatable' => $job->slug, 'value' => $response));
                }
            }
        }

        return $translations;
    }

    public static function getTranslationStatusList()
    {
        return array(
            'aucune'        => 'aucune demande',
            'queued'        => 'requête envoyé',
            'available'     => 'requête dispo',
            'pending'       => 'requête en cours de traitement',
            'reviewable'    => 'reviewable',
            'approved'      => 'approuvé',
            'revising'      => 'en revision',
            'rejected'      => 'rejeté',
            'canceled'      => 'annulé',
            'hold'          => 'en attente'
        );
    }

    public function getReviewableResponse() 
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        $responses = array();

        foreach ($order->jobs as $job) {
            $responses[$job->slug] =  $job->callbackResponses()->orderBy('created_at', 'desc')->first();
        }

        return $responses;
    }

    public function getApprovedTranslatableValues()
    {
        $approvedTranslatables = array();

        $approvedOrder = $this->translationRequestOrders()->where('status', 'approved')->orderby('created_at', 'desc')->first();
        if($approvedOrder) {
            foreach ($approvedOrder->jobs as $job) {
                $approvedTranslatables[$job->slug] = $job->callbackResponses()->orderBy('created_at', 'desc')->first()->body_tgt;
            }
        }

        return $approvedTranslatables;
    }

    public static function getApprovedTranslatableValueById($id)
    {
        $order = self::find($id);
        return $order->getApprovedTranslatableValues();
    }

    public function translationRequestOrders()
    {
        return $this->morphMany('Tokido\Dikateny\Model\TranslationRequestOrder', 'translatable');
    }

    public function applyTranslations($translations)
    {
        /*BuildKeyValue*/
        $toSave = array();

        foreach ($translations as $key => $translation) {
            if(array_key_exists($key, $this->applyTranslationTo)) {
                $current = $this->applyTranslationTo[$key];
                $this->$current = $translation;
            }
        }
        $this->update($toSave);
        $this->save();

        return $this;
    }
}
